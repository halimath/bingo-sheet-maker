package core

import (
	"fmt"
	"io"
	"math/rand"
	"path/filepath"
	"time"

	"github.com/jung-kurt/gofpdf"
)

const (
	paperWidth  = 297
	paperHeight = 210
	colWidth    = paperWidth/2.0 - 20.0
)

type Options struct {
	SheetSize      int
	NumberOfSheets int
	Words          []string
	RandomSeed     int64
}

type sheets struct {
	options        Options
	created        time.Time
	random         *rand.Rand
	boxWidth       float64
	pdf            *gofpdf.Fpdf
	textTranslator func(string) string
}

func newSheets(options Options) *sheets {
	creationDate := time.Now()

	pdf := gofpdf.New("L", "mm", "A4", "")
	pdf.SetTitle("Bingo", true)
	pdf.SetAuthor("", true)
	pdf.SetCreationDate(creationDate)
	pdf.SetCreator("Bingo Sheet Maker v0.2.0", true)
	pdf.SetFillColor(240, 240, 255)

	return &sheets{
		options:        options,
		created:        creationDate,
		random:         rand.New(rand.NewSource(options.RandomSeed)),
		boxWidth:       float64((paperWidth/2 - 20 - (options.SheetSize-1)*2) / options.SheetSize),
		pdf:            pdf,
		textTranslator: pdf.UnicodeTranslatorFromDescriptor(""),
	}
}

func (sh *sheets) appendWordListPage() {
	sh.pdf.AddPage()
	sh.pdf.SetFont("Arial", "B", 16)
	sh.pdf.SetX(10)
	sh.pdf.CellFormat(paperWidth, 20, sh.textTranslator("Liste aller Wörter"), "", 0, "C", false, 0, "")

	sh.pdf.SetFont("Arial", "", 10)
	x := 10
	y := 20
	for _, w := range sh.options.Words {
		if y+40 > paperHeight {
			x += 80
			y = 20
		}
		y += 5
		sh.pdf.SetY(float64(y))
		sh.pdf.SetX(float64(x))
		sh.pdf.CellFormat(colWidth, 10, sh.textTranslator(w), "", 0, "L", false, 0, "")
	}

	sh.pdf.SetY(paperHeight - 35)
	sh.pdf.SetX(10)
	sh.pdf.SetFont("Arial", "", 8)
	sh.pdf.CellFormat(paperWidth-20, 10, fmt.Sprintf("Erstellt mit http://bingo-sheet-maker.wilanthaou.de am %s", sh.created.Format("01.02.2006, 15:04:05")), "", 0, "R", false, 0, "http://bingo-sheet-maker.wilanthaou.de")
}

func (sh *sheets) appendSheetsPage() {
	sh.pdf.AddPage()
	sh.pdf.Line(paperWidth/2, 10, paperWidth/2, paperHeight-10)

	for s := 0; s < 2; s++ {
		sh.pdf.SetFont("Arial", "B", 16)
		// pdf.Line(paperWidth/2*float64(s)+10.0, 10, paperWidth/2*float64(s+1)-10.0, 10)
		sh.pdf.SetY(10)
		sh.pdf.SetX(float64(s)*paperWidth/2 + 10)
		sh.pdf.CellFormat(colWidth, 20, "Bingo", "", 0, "C", false, 0, "")

		w := sh.shuffledWords()

		i := 0
		for r := 0; r < sh.options.SheetSize; r++ {
			for c := 0; c < sh.options.SheetSize; c++ {
				roundedRectFilled(sh.pdf, paperWidth/2.0*float64(s)+10.0+(sh.boxWidth+2)*float64(c), 40+(sh.boxWidth+2)*float64(r), sh.boxWidth, sh.boxWidth, 2)
				sh.pdf.SetY(40 + (sh.boxWidth+2)*float64(r))
				sh.pdf.SetX(paperWidth/2.0*float64(s) + 10.0 + (sh.boxWidth+2)*float64(c))

				if r == sh.options.SheetSize/2 && c == sh.options.SheetSize/2 {
					sh.writeStar()
				} else {
					sh.pdf.SetFont("Arial", "", 12)
					sh.pdf.CellFormat(sh.boxWidth, sh.boxWidth, sh.textTranslator(w[i]), "", 0, "CM", false, 0, "")
					i++
				}
			}
		}

		sh.pdf.SetY(paperHeight - 35)
		sh.pdf.SetX(float64(s)*paperWidth/2 + 10)
		sh.pdf.SetFont("Arial", "", 8)
		sh.pdf.CellFormat(colWidth, 10, fmt.Sprintf("Erstellt mit http://bingo-sheet-maker.wilanthaou.de am %s", sh.created.Format("01.02.2006, 15:04:05")), "", 0, "R", false, 0, "http://bingo-sheet-maker.wilanthaou.de")
	}
}

func (sh *sheets) shuffledWords() []string {
	w := make([]string, len(sh.options.Words))
	copy(w, sh.options.Words)

	ret := make([]string, len(sh.options.Words))
	n := len(sh.options.Words)
	for i := 0; i < n; i++ {
		randIndex := sh.random.Intn(len(w))
		ret[i] = w[randIndex]
		w = append(w[:randIndex], w[randIndex+1:]...)
	}
	return ret
}

func (sh *sheets) writeStar() {
	// sh.pdf.SetFont("Arial", "B", 40)
	// sh.pdf.CellFormat(sh.boxWidth, sh.boxWidth, "*", "", 0, "CM", false, 0, "")

	var opt gofpdf.ImageOptions
	opt.ImageType = "png"
	sh.pdf.ImageOptions(filepath.Join(".", "assets", "star.png"), sh.pdf.GetX(), sh.pdf.GetY(), sh.boxWidth, sh.boxWidth, false, opt, 0, "")
}

func GenerateBingoSheetsPDF(options Options, w io.Writer) error {
	sheets := newSheets(options)

	for p := 0; p < options.NumberOfSheets/2; p++ {
		sheets.appendSheetsPage()
	}

	sheets.appendWordListPage()

	return sheets.pdf.Output(w)
}

func roundedRectFilled(pdf *gofpdf.Fpdf, x, y, w, h, radius float64) {
	pdf.ClipRoundedRect(x, y, w, h, radius, true)
	pdf.Rect(x, y, w, h, "F")
	pdf.ClipEnd()
}
