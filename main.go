package main

import (
	"net/http"

	"bitbucket.org/halimath/bingo-sheet-maker/web"
	"bitbucket.org/halimath/kvlog"
)

func main() {
	kvlog.Info(kvlog.KV("event", "started"))
	http.ListenAndServe(":8000", kvlog.Handler(kvlog.L, web.Mux))
}

func index(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("test"))
}
