FROM golang:1.12-alpine

RUN apk add git

RUN mkdir -p /app

ADD assets/ /app/assets
ADD core/ /app/core
ADD static/ /app/static
ADD web/ /app/web
ADD go.mod /app/
ADD go.sum /app/
ADD main.go /app/

WORKDIR /app
RUN go build

EXPOSE 8000

ENTRYPOINT [ "./bingo-sheet-maker" ]
