package web

import (
	"fmt"
	"log"
	"net/http"
	"path"
	"strconv"
	"strings"

	"bitbucket.org/halimath/bingo-sheet-maker/core"
)

var Mux = http.NewServeMux()

func init() {
	Mux.HandleFunc("/pdf", generatePDF)
	Mux.Handle("/", http.FileServer(http.Dir(path.Join(".", "static"))))
}

func generatePDF(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		http.Error(w, fmt.Sprintf("Failed to read body: %s", err), 500)
		return
	}

	h := w.Header()
	h.Add("Content-Type", "application/pdf")
	h.Add("Content-Disposition", "attachment; filename=\"bingo.pdf\"")

	sheetSize, err := strconv.Atoi(r.FormValue("sheetSize"))
	if err != nil {
		http.Error(w, fmt.Sprintf("Invalid sheet size: %s", err), 500)
		return
	}

	numberOfSheets, err := strconv.Atoi(r.FormValue("numberOfSheets"))
	if err != nil {
		http.Error(w, fmt.Sprintf("Invalid number of sheets: %s", err), 500)
		return
	}

	randomSeed, err := strconv.ParseInt(r.FormValue("randomSeed"), 10, 64)
	if err != nil {
		http.Error(w, fmt.Sprintf("Invalid random seed: %s", err), 500)
		return
	}

	err = core.GenerateBingoSheetsPDF(core.Options{
		SheetSize:      sheetSize,
		NumberOfSheets: numberOfSheets,
		Words:          strings.Split(strings.TrimSpace(r.FormValue("words")), "\n"),
		RandomSeed:     randomSeed,
	}, w)

	if err != nil {
		// TODO: Send error
		log.Panic(err)
		return
	}
}
