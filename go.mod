module bitbucket.org/halimath/bingo-sheet-maker

go 1.12

require (
	bitbucket.org/halimath/kvlog v0.1.0
	github.com/jung-kurt/gofpdf v1.5.4
)
